import React from "react";
import {Surface} from "react-canvas";
import Sky from "./components/sky";

class App extends React.Component {
  render() {
    var surfaceWidth = window.innerWidth - 20;
    var surfaceHeight = window.innerHeight - 20;
    return (
      <Surface width={surfaceWidth} height={surfaceHeight} top={0} left={0}>
        <Sky width={surfaceWidth} height={surfaceHeight} stars={250}/>
      </Surface>
    );
  }
}
React.render(<App />, document.body);

window.addEventListener("source", () => {
  React.render(<App />, document.body)
});
