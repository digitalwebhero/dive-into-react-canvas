'use strict';

import React from 'react';
import StarLayer from './starLayer';

export default class Star extends React.Component {
    constructor(props) {
        super(props);
        this.state = this.props;
        setInterval(this.lifecicle.bind(this), 10000);
    }

    componentDidMount() {
        this.lifecicle();
    }

    lifecicle() {
        this.setState({
            moveTo: {
                left: randomNumber(0, this.props.maxLeft),
                top: randomNumber(0, this.props.maxTop)
            }
        }, function(){
            setInterval(this.move.bind(this), 60/100);
        }.bind(this));
    }

    move(){
        var leftDir = this.state.left > this.state.moveTo.left ? -1 : 1;
        var topDir = this.state.top > this.state.moveTo.top ? -1 : 1;

        if (this.state.left === this.state.moveTo.left) {
            leftDir = 0;
        }

         if (this.state.top === this.state.moveTo.top) {
            topDir = 0;
        }

        this.setState({
            left: this.state.left + leftDir,
            top: this.state.top + topDir
        });
    }

    render(){
        return <StarLayer {...this.state}/>
    }
}

function randomNumber(from, to) {
  return Math.floor((Math.random() * to) + from);
}