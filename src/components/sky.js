import { Group } from "react-canvas";
import Star from "./star";
import React from "react";

export default class Sky extends React.Component {
  render() {
    var style = {
      width: this.props.width,
      height: this.props.height,
      backgroundColor: "#000",
      zIndex: 3,
      alpha: 1
    },
    stars = this.renderStars();
    return (
      <Group style={style}>
        {stars}
      </Group>
    );
  }

  renderStars() {
    var stars = [];
    for (var i = 0; i < this.props.stars; i++) {
      stars.push(
        <Star key={i}
              top={randomNumber(0, this.props.height)}
              left={randomNumber(0, this.props.width)}
              maxTop={this.props.height}
              maxLeft={this.props.width}/>
      );
    };
    return stars;
  }
}

function randomNumber(from, to) {
  return Math.floor((Math.random() * to) + from);
}