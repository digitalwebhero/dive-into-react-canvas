'use strict';

import React from 'react';
import createComponent from 'react-canvas/lib/createComponent';
import LayerMixin from 'react-canvas/lib/LayerMixin';

export default createComponent('Star', LayerMixin, {
  mountComponent: function (rootID, transaction, context) {
    var props = this._currentElement.props;
    var layer = this.node;
    layer.type = "custom";
    this.applyLayerProps({}, props);
    layer.zIndex = 99;
    layer.customDrawFunc = function(ctx, node) {
        return star(ctx, props.left, props.top, 10, 5, 0.5);
    };
    return layer;
  },

  receiveComponent: function (nextComponent, transaction, context) {
    var prevProps = this._currentElement.props;
    var props = nextComponent.props;
    var layer = this.node;
    this.applyLayerProps({}, props);
    layer.customDrawFunc = function(ctx, node) {
        return star(ctx, props.left, props.top, 10, 5, 0.5);
    };
    this._currentElement = nextComponent;
    this.node.invalidateLayout();
  }

});

function star(ctx, x, y, r, p, m) {
    ctx.save();
    ctx.beginPath();
    ctx.translate(x, y);
    ctx.moveTo(0,0-r);
    for (var i = 0; i < p; i++)
    {
        ctx.rotate(Math.PI / p);
        ctx.lineTo(0, 0 - (r*m));
        ctx.rotate(Math.PI / p);
        ctx.lineTo(0, 0 - r);
    }
    ctx.strokeStyle = "#000";
    ctx.fillStyle = "#ff0";
    ctx.lineWidth = 1;
    ctx.fill();
    ctx.stroke();
    ctx.restore();
}